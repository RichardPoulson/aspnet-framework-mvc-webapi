# [ASP.NET](https://dotnet.microsoft.com/apps/aspnet ".NET is a developer platform made up of tools, programming languages, and libraries for building many different types of applications.  ASP.NET extends the .NET developer platform with tools and libraries specifically for building web apps.") [Framework](https://docs.microsoft.com/en-us/dotnet/framework/ "The .NET Framework is a development platform for building apps for web, Windows, Windows Phone, Windows Server, and Microsoft Azure. It consists of the common language runtime (CLR) and the .NET Framework class library, which includes a broad range of functionality and support for many industry standards.") web application and [API](https://dotnet.microsoft.com/apps/aspnet/apis "ASP.NET makes it easy to build services that reach a broad range of clients, including browsers and mobile devices.  With ASP.NET you use the same framework and patterns to build both web pages and services, side-by-side in the same project.").
## Targets [.NET Framework 4.7.2](https://dotnet.microsoft.com/download/dotnet-framework/net472)

## Opening This Project on Your Computer:
1. First, you may want to [fork](https://help.github.com/en/articles/fork-a-repo) (make a copy of) this project to your GitHub profile. 
2. Next, make sure that you have the newest release of [.NET Framework 4](https://dotnet.microsoft.com/download/dotnet-framework) installed.
3. Clone the project:
    * __Windows__:
      1. After opening VS, look for the __Get started__ section and click the __Clone or check out code__ button.
      2. In the __Repository location__ field, paste the web URL for [this project](https://github.com/RichardPoulson/aspnet-framework-mvc-webapi.git) __or__ your fork of the project.  Change the local path of the clone if you want, and click the __clone__ button to continue.
      3. After VS clones the project, look for a file named "asp-dotnet-framework-mvc.sln" in the __Solution Explorer__ view (If the view doesn't open automatically, go to __View__ -> __Solution Explorer__ in the VS menu bar).  Right-click on the file, and select the __Open Folder in File Explorer__ option.  After File Explorer finishes opening, close the current instance of VS.
      4. Inside File Explorer, __double-click asp-dotnet-framework-mvc.sln__ to finish importing the [solution](https://docs.microsoft.com/en-us/visualstudio/extensibility/internals/solution-dot-sln-file?view=vs-2019). After VS finishes importing the solution, click on __File__ -> __Close Solution__ in the VS menu bar.
      5. This will take us back to the VS welcome screen.  In the __Open Recent__ section, you should see __two entries__ for asp-dotnet-framework-mvc; one with a folder icon and the other with a VS solution icon.  __Right-click__  on the entry __with the folder icon__ and select __Remove From List__ (we want a link to the solution, not the folder).  Now you can use the remaining link to open your VS solution for the web app.

## Running/Debugging the Project:
__Note:__ This web app utilizes HTTPS, so you will need to install/trust a security certificate (for VS's built-in web server) the first time that you run the web application.
1. Run the web application:
    * __Windows__:
      1. In the VS menu bar click on __Debug__ -> __Start Without Debugging__.


## Testing the Project:
1. Run the web application:
    * __Windows__:
      1. In the VS menu bar click on __Test__ -> __Windows__ -> __Test Explorer__.
      2. 


## Helpful Websites:
* [GitLab CI/CD with .Net Framework - Medium.com](https://medium.com/@gabriel.faraday.barros/gitlab-ci-cd-with-net-framework-39220808b18fv)
